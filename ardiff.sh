#!/bin/ksh

LC_ALL=C
export LC_ALL

#       	     _ _  __  __ 
#   __ _ _ __ __| (_)/ _|/ _|
#  / _` | '__/ _` | | |_| |_ 
# | (_| | | | (_| | |  _|  _|
#  \__,_|_|  \__,_|_|_| |_|  
#
# Ardiff:
# Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz, 138497
# Projekt 1a, IOS 2010/2011
				                              
#zaloha aktualniho scriptu do adresare ./zalohy
jmeno_zalohy=ardiff_$(date +%Y-%m-%d)_$(date +%H-%M-%S)
tar czf ./zalohy/$jmeno_zalohy.tar.gz ardiff.sh

# funkce vypisuje napovedu
function printHelp {
echo 'ardiff vypisuje zmeny archivu, vytvari rozdilovy archiv nebo aplikuje rozdilovy
 archiv na zdrojovy archiv.
 Pouziti: ardiff [volby] archiv1 archiv2
 Volby:
  -o SOUBOR  Pokud je cilem skriptu vytvorit archiv, bude vytvoren do souboru
     se jmenem SOUBOR (plati pro -c a -p).
  -l         Vypis seznamu souboru, ktere se v zadanych archivech lisi.
  -c         Vytvoreni rozdiloveho archivu.
  -p         Aplikace rozdiloveho archivu (argument archiv2) na zdrojovy archiv
             (argument archiv1).
  -r         Prepnuti do reverzniho rezimu (plati pro -p).3	'
}

# zjisti typ a dekomprimuje archiv $1 do adresare $2
function decomprimeToTemp {
	archive=$1
	tmp_dir=$2
	archive_type=`file $archive | sed 's/^[^:]*//' | grep "bzip2"`
	if [ ! "$archive_type" = "" ]
	then
		#echo "je to bzip2"
		tar -xjf $archive -C $tmp_dir
	else
		archive_type=`file $archive | sed 's/^[^:]*//' | grep "gzip"`
		if [ ! "$archive_type" = "" ]
		then
			#echo "je to gzip"
			tar -xzf $archive -C $tmp_dir
		else
			archive_type=`file $archive | sed 's/^[^:]*//' | grep -i "Zip"`
			if [ ! "$archive_type" = "" ]
			then
				#echo "je to Zip"
				unzip -q $archive -d $tmp_dir
			else
				archive_type=`file $archive | sed 's/^[^:]*//' | grep "tar"`
				if [ ! "$archive_type" = "" ]
				then
					#echo "je to tar"
					tar -xf $archive -C $tmp_dir
				else
					echo "nepodporovany archiv: $archive" >&2
					exit 1
				fi
			fi
		fi
	fi
}

function diffRecursive {
	local old_dir=$1
	local new_dir=$2
	local line=""
	diff --text -q "$old_dir" "$new_dir" |
	while read line; 
	do
		# KDYZ SE LISI 2 SOUBORY
		local pom=`printf "$line" | sed 's/.*\(differ\)$/\1/' `
		if [ "$pom" = "differ" ]
		then
			#pom=`printf "$line" | sed "s@Files $old_dir.* and $new_dir\/\(.*\) differ@\1@" `
			pom=`printf "$line" | sed "s@.* $new_tmp_dir\/\(.*\) differ@\1@" `
			echo $pom		
		else
			# KDYZ JE V JEDNOM DIR A V DRUHYM FILE
			pom=`printf "$line" | sed 's/^\(File \).*/\1/' `
			if [ "$pom" = "File " ]
			then
				#pom=`printf "$line" | sed "s@ \($old_dir\/.*\) is a directory while.*@\1@" `
				pom=`printf "$line" | sed "s@.* $new_dir\/\(.*\) is a regular file@\1@" `	
				#pom=`printf "$line" | sed "s@File $old_dir\/\(.*\) is a directory while file $new_dir.*@\1@" `
				#echo @"$pom"@
				if [ "$pom" = "$line" ]
				then
					pom=`printf "$line" | sed "s@.* $new_dir\/\(.*\) is a directory@\1@" `	
				fi
				#echo "--$pom"

				if [ -d $new_dir/$pom ]
				then
					echo `printf "$new_dir/$pom" | sed "s@$new_tmp_dir\/\(.*\)@\1@" `
					diffRecursive $patch_tmp_dir $new_dir/$pom 
				else
					echo `printf "$old_dir/$pom" | sed "s@$old_tmp_dir\/\(.*\)@\1@" `
					diffRecursive $patch_tmp_dir $old_dir/$pom
				fi

			else
				# KDYZ JE JENOM V JEDNOM
				pom=`printf "$line" | sed 's/^\(Only in\).*/\1/' `
				if [ "$pom" = "Only in" ]
				then
					pom=`printf "$line" | sed "s@^Only in $new_dir: \(.*\)@\1@" `
					local path=$new_dir
					if [ "$pom" = "$line" ]
					then
						pom=`printf "$line" | sed "s@^Only in $old_dir: \(.*\)@\1@" `
						path=$old_dir
					fi
					#echo "++$pom"

					# kdyz je to soubor
					if [ -f $path/$pom ]
					then
						#echo +f+$path/$pom
						local path_ia=`printf "$path/$pom" | sed "s@$new_tmp_dir/\(.*\)@\1@" `
						if [ "$path_ia" = "$path/$pom" ]
						then
							path_ia=`printf "$path/$pom" | sed "s@$old_tmp_dir/\(.*\)@\1@" `
							echo $path_ia #+f+
						else
							echo $path_ia #+f+
						fi
					else
						# kdyz je to adresar
						if [ -d $path/$pom ]
						then
							#echo +d+$path/$pom
							diffRecursive $patch_tmp_dir $path/$pom
						fi
					fi
				fi
			fi

		fi
		#echo "$line"

	done
	
	# rekurze pro vsechny stejne adresare
	soucasny=`pwd`
	cd $new_dir
	#echo `pwd`
	ls -dp * 2> /dev/null | grep / | sed 's/\///' |
	while read line 
	do
		if [ -d $old_dir/$line ]&&[ -d $new_dir/$line ]
		then
			#echo $line
			diffRecursive $old_dir/$line $new_dir/$line

		fi

	done
	cd $soucasny
	#echo `pwd`


}


function diffRecursiveCreateDiff {
	local old_dir=$1
	local new_dir=$2
	local line=""
	diff --text -q "$old_dir" "$new_dir" |
	while read line; 
	do
		# KDYZ SE LISI 2 SOUBORY
		local pom=`printf "$line" | sed 's/.*\(differ\)$/\1/' `
		if [ "$pom" = "differ" ]
		then
			#pom=`printf "$line" | sed "s@Files $old_dir.* and $new_dir\/\(.*\) differ@\1@" `
			pom=`printf "$line" | sed "s@.* $new_tmp_dir\/\(.*\) differ@\1@"`
			local cesta=`dirname "$pom"`
			if [ ! -d "$patch_tmp_dir/$cesta" ] 
			then
				    mkdir -p "$patch_tmp_dir/$cesta"
			fi
				
			#local radek=""
			#local radek2=""
			diff -u "$new_tmp_dir/$pom" "$old_tmp_dir/$pom" > "$patch_tmp_dir/$pom.patch"
			sed -i "s@$new_tmp_dir@b@" "$patch_tmp_dir/$pom.patch"
 			sed -i "s@$old_tmp_dir@a@" "$patch_tmp_dir/$pom.patch"
		else
			# KDYZ JE V JEDNOM DIR A V DRUHYM FILE
			pom=`printf "$line" | sed 's/^\(File \).*/\1/' `
			if [ "$pom" = "File " ]
			then
				#pom=`printf "$line" | sed "s@ \($old_dir\/.*\) is a directory while.*@\1@" `
				pom=`printf "$line" | sed "s@.* $new_dir\/\(.*\) is a regular file@\1@" `	
				#pom=`printf "$line" | sed "s@File $old_dir\/\(.*\) is a directory while file $new_dir.*@\1@" `
				#echo @"$pom"@
				if [ "$pom" = "$line" ]
				then
					pom=`printf "$line" | sed "s@.* $new_dir\/\(.*\) is a directory@\1@" `	
				fi
				#echo "--$pom"
				
				local cesta=`printf "$new_dir/" | sed "s@$new_tmp_dir\/\(.*\)@\1@" `	
				#echo "|$cesta"
				#cesta=`dirname "$cesta"`
				if [ ! -d "$patch_tmp_dir/$cesta" ] 
				then
			    	mkdir -p "$patch_tmp_dir/$cesta"
				fi
			
				#diff -u "$new_tmp_dir/$cesta$/$pom" "$old_tmp_dir/$cesta/$pom" > "$patch_tmp_dir/$cesta/$pom.patch"
				#sed -i "s@$new_tmp_dir@b@" "$patch_tmp_dir/$cesta/$pom.patch"
 				#sed -i "s@$old_tmp_dir@a@" "$patch_tmp_dir/$cesta/$pom.patch"

				if [ -d $new_dir/$pom ]
				then
					#echo `printf "$new_dir/$pom" | sed "s@$new_tmp_dir\/\(.*\)@\1@" `
					#echo "$new_dir $pom"
					diff -u -a "$old_dir/$pom" "$prazdny_soubor" > "$patch_tmp_dir/$cesta/$pom.patch"
					#cesta=$cesta/
					sed -i "s@$prazdny_soubor@b/$cesta$pom@" "$patch_tmp_dir/$cesta$pom.patch"
 					sed -i "s@$old_tmp_dir@a@" "$patch_tmp_dir/$cesta$pom.patch"
					
					diffRecursiveCreateDiff $prazdny_adresar $new_dir/$pom 
				else
					#echo `printf "$old_dir/$pom" | sed "s@$old_tmp_dir\/\(.*\)@\1@" `
					#echo "$new_dir $pom"
					diff -u -a "$prazdny_soubor" "$new_dir/$pom" > "$patch_tmp_dir/$cesta/$pom.patch"
					#cesta=$cesta/
					sed -i "s@$prazdny_soubor@a/$cesta$pom@" "$patch_tmp_dir/$cesta$pom.patch"
 					sed -i "s@$new_tmp_dir@b@" "$patch_tmp_dir/$cesta$pom.patch"
					diffRecursiveCreateDiff $prazdny_adresar $old_dir/$pom
				fi

			else
				# KDYZ JE JENOM V JEDNOM
				pom=`printf "$line" | sed 's/^\(Only in\).*/\1/' `
				if [ "$pom" = "Only in" ]
				then
					pom=`printf "$line" | sed "s@^Only in $new_dir: \(.*\)@\1@" `
					local path=$new_dir
					if [ "$pom" = "$line" ]
					then
						pom=`printf "$line" | sed "s@^Only in $old_dir: \(.*\)@\1@" `
						path=$old_dir
					fi
					#echo "++$pom"

					# kdyz je to soubor
					if [ -f $path/$pom ]
					then
						#echo +f+$path/$pom
						local path_ia=`printf "$path/$pom" | sed "s@$new_tmp_dir/\(.*\)@\1@" `
						if [ "$path_ia" = "$path/$pom" ]
						then
							path_ia=`printf "$path/$pom" | sed "s@$old_tmp_dir/\(.*\)@\1@" `
						fi		
						
							
						
						
						path_ia=`dirname "$path_ia"`
						#echo "_$path_ia"
						if [ ! -d "$patch_tmp_dir/$path_ia" ] 
						then
			    			mkdir -p "$patch_tmp_dir/$path_ia"
						fi

						diff -u -a "$path/$pom" "$prazdny_soubor" > "$patch_tmp_dir/$path_ia/$pom.patch"
						#cesta=$cesta/
						if [ -f $new_tmp_dir/$path_ia/$pom ]
						then
							sed -i "s@$prazdny_soubor@a/$path_ia/$pom@" "$patch_tmp_dir/$path_ia/$pom.patch"
 							sed -i "s@$new_tmp_dir@b@" "$patch_tmp_dir/$path_ia/$pom.patch"
						else
							sed -i "s@$prazdny_soubor@b/$path_ia/$pom@" "$patch_tmp_dir/$path_ia/$pom.patch"
 							sed -i "s@$old_tmp_dir@a@" "$patch_tmp_dir/$path_ia/$pom.patch"
						fi

					else
						# kdyz je to adresar
						if [ -d $path/$pom ]
						then
							#echo +d+$path/$pom
							diffRecursiveCreateDiff $prazdny_adresar $path/$pom
						fi
					fi
				fi
			fi

		fi
		#echo "$line"

	done
	
	# rekurze pro vsechny stejne adresare
	soucasny=`pwd`
	cd $new_dir
	ls -dp * 2> /dev/null | grep / | sed 's/\///' |
	while read line 
	do
		if [ -d $old_dir/$line ]&&[ -d $new_dir/$line ]
		then
			diffRecursiveCreateDiff $old_dir/$line $new_dir/$line

		fi

	done
	cd $soucasny


}

# --------------------------------------------------- MAIN ---------------------------------------------------------------------

# kdyz nejsou zadny argumenty, vypise se napoveda
if [ $# -eq 0 ]
then
	printHelp
	exit 1
fi


# vynulovani vsech flagu
l_flag=0
c_flag=0
p_flag=0
r_flag=0
o_flag=0
output=''

# projiti parametru getopts, kdyz je nejaky flag nalezen, je dano flag++
while getopts ":cprlo:" opt; 
do
	case $opt in
		l)	#echo "parametr: $opt" >&2
			l_flag=$(( $l_flag + 1 ))
	      	;;

		c)	#echo "parametr: $opt" >&2
			c_flag=$(( $c_flag + 1 ))
	      	;;

		p)	#echo "parametr: $opt" >&2
			p_flag=$(( $p_flag + 1 ))
	      	;;

		r)	#echo "parametr: $opt" >&2
			r_flag=$(( $r_flag + 1 ))
	      	;;

		o)	#echo "parametr: $opt" >&2
			o_flag=$(( $o_flag + 1 ))
			#echo "output: $OPTARG"
			output=$OPTARG
	      	;;

     	\?) echo "Invalid option: -$OPTARG" >&2
			#printHelp
	      	exit 1
		  	;;
	  	:)	echo "Option -$OPTARG requires an argument." >&2
			#printHelp
			exit 1
			;;
    esac
	
done

# kontrola neplatneho krizeni zadanych parametru
if [ $(( $l_flag + $c_flag + $p_flag)) -gt 1 ]||[ $(( $l_flag + $c_flag + $p_flag)) -lt 1 ]
then
	echo "nepripustne krizeni parametru" >&2
	exit 1
fi

if [ $(( $l_flag + $o_flag )) -gt 1 ]
then
	echo "nepripustne krizeni parametru" >&2
	exit 1
fi

if [ $(( $l_flag + $r_flag )) -gt 1 ]
then
	echo "nepripustne krizeni parametru" >&2
	exit 1
fi

if [ $(( $c_flag + $r_flag )) -gt 1 ]
then
	echo "nepripustne krizeni parametru" >&2
	exit 1
fi

# odchytava navic zadane argumenty
pocet_par=$(( $l_flag + $c_flag + $p_flag + $r_flag + $((2*$o_flag)) + 2)) 
if [ $# -ne "$pocet_par" ]||[ $# -eq 2 ]
then
    echo "spatny pocet parametru" >&2
	exit 1
fi

# ziskani jmen archivu, se kterymi se bude pracovat
params=("$@")
new_archive=${params[$(($pocet_par - 1))]}
old_archive=${params[$(($pocet_par - 2))]}


# IFy co se vlastne bude delat


# VYPSANI ROZDILU
if [ $l_flag -eq 1 ]||[ $c_flag -eq 1 ]
then
	# kontrola existence noveho archivu
	if [ ! -f $new_archive ]
	then
		echo "soubor neexistuje: $new_archive" >&2
		exit 1
	fi
	# kontrola existence stareho archivu
	if [ ! -f $old_archive ]
	then
		echo "soubor neexistuje: $old_archive" >&2
		exit 1
	fi

	# vytvoreni temp adresare v /tmp/
	new_tmp_dir=`mktemp -d /tmp/ardiff_XXXXXXXXXX`
	if [ ! -d $new_tmp_dir ]
	then
		echo "nelze vytvorit pomocny adresar v adresari /tmp/" >&2
		exit 1
	fi
	
	old_tmp_dir=`mktemp -d /tmp/ardiff_XXXXXXXXXX`
	if [ ! -d $old_tmp_dir ]
	then
		echo "nelze vytvorit pomocny adresar v adresari /tmp/" >&2
		exit 1
	fi
	
	patch_tmp_dir=`mktemp -d /tmp/ardiff_XXXXXXXXXX`
	prazdny_adresar=`mktemp -d /tmp/ardiff_XXXXXXXXXX`
	prazdny_soubor=`mktemp /tmp/ardiff_XXXXXXXXXX`

	# dekomprimace archivu
	decomprimeToTemp $new_archive $new_tmp_dir
	decomprimeToTemp $old_archive $old_tmp_dir

	if [ $l_flag -eq 1 ]
	then
		diffRecursive $old_tmp_dir $new_tmp_dir
	fi

	if [ $c_flag -eq 1 ]
	then
		diffRecursiveCreateDiff $old_tmp_dir $new_tmp_dir

		typ=`echo "$output" | grep "tar.gz"`
		if [ "$typ" = "$output" ]
		then 
			aktualni=`pwd`
			cd $patch_tmp_dir
			tar -czf $output.tar.gz *
			cd "$aktualni"
			mv $patch_tmp_dir/$output.tar.gz ./$output.tar.gz
		else
			typ=`echo "$output" | grep "tar.bz2"`
			if [ "$typ" = "$output" ]
			then 
				aktualni=`pwd`
				cd $patch_tmp_dir
				tar -cjf $output.tar.bz2
				cd "$aktualni"
				mv $patch_tmp_dir/$output.tar.bz2 ./$output.tar.bz2
			fi
		fi


	fi

fi

if [ $p_flag -eq 1 ]
then
	echo "Neimplementovano"
fi

#echo OK
